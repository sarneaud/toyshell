# https://dlang.org/download.html

toyshell: toyshell.d
	dmd toyshell

test: toyshell
	./test

.PHONY: test
