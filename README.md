# Toyshell
A simple toy shell interpreter that's not entirely unlike a POSIX Bourne shell.

It's meant to be a few hundred lines of easily readable code that explains how a shell works.  Even though it's just a toy, if you understand this shell, you'll understand

* The fork/exec pattern and why it's useful
* How redirection works
* What builtins are and why they're needed
* What `export` even does
* How word expansion works

Comments explain some things that aren't implemented, like background commands and pipelines.  [There's a detailed walkthrough on my blog.](https://theartofmachinery.com/2018/11/07/writing_a_nix_shell.html)

## What Works
[A real POSIX-compliant shell has a lot of features](http://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html), and most of them aren't implemented by Toyshell.

Here's a demo:
```
/tmp/toydemo > # Comments are supported
/tmp/toydemo > # So is I/O redirection (note the syntax)
/tmp/toydemo > whoami >1-noun.txt
/tmp/toydemo > # You can use shell variables and environment variables
/tmp/toydemo > VERB=eats
/tmp/toydemo > # Variable substitution is also supported (only with ${} syntax)
/tmp/toydemo > echo ${VERB} >2-verb.txt
/tmp/toydemo > # You can pass environment variables to commands to mess with them
/tmp/toydemo > LD_DEBUG=statistics rev <1-noun.txt >3-object.txt
     15483:
     15483:     runtime linker statistics:
     15483:       total startup time in dynamic loader: 2521832 cycles
     15483:                 time needed for relocation: 835280 cycles (33.1%)
     15483:                      number of relocations: 130
     15483:           number of relocations from cache: 3
     15483:             number of relative relocations: 1278
     15483:                time needed to load objects: 1074076 cycles (42.5%)
/tmp/toydemo > ls
1-noun.txt  2-verb.txt  3-object.txt
/tmp/toydemo > # Simple "*" globbing is supported (and nothing else)
/tmp/toydemo > cat *
kay
eats
yak
/tmp/toydemo > exit
```

That's pretty much it, but read the source for full details.

## History and Command Editing
If you want nicer command line editing, run Toyshell with [`rlwrap`](https://github.com/hanslub42/rlwrap) (available in most package managers).  If you're writing your own shell, using an existing library like [GNU Readline](https://cnswww.cns.cwru.edu/php/chet/readline/readline.html) is better than rolling your own.

## Building
Toyshell's written in [D](https://dlang.org).  It's a single file with no dependencies, so you should be able to compile directly with any [D compiler](https://dlang.org/download.html), e.g.:

```
dmd toyshell
```

The test suite requires [`expect`](https://core.tcl.tk/expect/index).  It's in the package manager of most distros.  NB: it creates and deletes files using Toyshell, which is free software with no warranty.

```
./test
```

Because Toyshell doesn't support quoting, the tests will fail if the current directory contains spaces or other special characters.
