/* Copyright Simon Arneaud, 2018
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
import core.sys.posix.fcntl;
import core.sys.posix.stdio;
import core.sys.posix.stdlib;
import core.sys.posix.sys.wait;
import core.sys.posix.unistd;
import std.algorithm;
import std.array;
import std.conv;
import std.exception;
import std.file;
import std.process : environment;
import io = std.stdio;
import std.string;
import std.typecons;

// http://tldp.org/LDP/abs/html/exitcodes.html
enum ErrorCode
{
	kOther = 1,
	kBuiltinUsage = 2,
	kNotExecutable = 126,
	kCommandNotFound = 127,
	kExitBadArg = 128,
}

struct Var
{
	string value;
	// An exported variable gets passed into the environment of children
	Flag!"exported" exported;
}
alias Var[string] VarMap;

struct CommandLine
{
	// By convention, the first argument is the command name
	string[] args;
	// Only standard input and output redirection supported for parsing simplicity
	string[2] io_redir_names = [null, null];
	VarMap tmp_env_vars;
}
alias builtin = int function(ref const(CommandLine) cmd, VarMap vars);

void main()
{
	// This stores shell variables (as opposed to environment variables)
	// Shell variables don't get inherited by child jobs.
	VarMap vars = ["?": Var("0")];

	writePrompt();
	foreach (line; io.stdin.byLineCopy())
	{
		auto command = parse(line, vars);
		auto last_status = runCommand(command, vars);
		vars["?"].value = last_status.to!string();
		writePrompt();
	}

	// You might have noticed that programs running in a shell usually quit when the shell does.
	// Child processes don't just automatically die when the parent dies.
	// A POSIX shell tracks all child processes and sends them a SIGHUP before it exits.
	// Most programs don't handle this signal, so the kernel ends up just killing them.
	// Programs will keep running after the shell exits if any of the following are true:
	// * They have code to handle the SIGHUP without quitting (e.g., lighttpd)
	// * They're wrapped in a program like nohup, or tmux or screen that handles the SIGHUP
	// * The disown builtin was used to tell the shell not to SIGHUP the job

	// This shell only runs one command at a time, synchronously, so nothing needs to be done here.
}

void writePrompt()
{
	io.write(getcwd());
	io.write(" > ");
}

// Confusingly, a POSIX shell has one syntax for evaluating a variable, but not just one kind of variable.
// This function keeps the lookup consistent.
string getVarValue(string name, const(VarMap) shell_vars, const(VarMap) tmp_vars = VarMap.init)
{
	if (auto var = name in tmp_vars)
	{
		return var.value;
	}
	if (auto var = name in shell_vars)
	{
		return var.value;
	}
	return environment.get(name, "");
}

CommandLine parse(string line, VarMap vars)
{
	CommandLine cmd_line;

	// A POSIX shell takes your command and does a complex series of processing before executing it.
	// This a grossly simplified version.  The biggest difference is the lack of quoting rules.

	// Only ${VAR} syntax is supported for variable expansion.
	// Values can come from either the environment or shell variables.
	import std.regex : Captures, regex, replaceAll;
	string variableInterpolator(Captures!string c)
	{
		auto name = c[1];
		return getVarValue(name, vars);
	}
	line = line.replaceAll!variableInterpolator(regex(`\$\{([^}]*)\}`));

	auto pieces = line.split();

	// You can assign a shell variable with a command like "VAR=42".
	// This non-POSIX-compliant toy shell implements that by rewriting to "= VAR 42" and executing it as a builtin.
	if (pieces.length == 1) if (auto assignment_pieces = pieces[0].findSplit("="))
	{
		cmd_line.args = ["=", assignment_pieces[0], assignment_pieces[2]];
		return cmd_line;
	}

	auto args_app = appender(&cmd_line.args);
	foreach (piece; pieces)
	{
		if (piece.startsWith("#"))
		{
			// Ignore the rest of the line if we see a comment
			break;
		}

		// A command line can start with a series of temporary environment variables, e.g.:
		// SDL_VIDEODRIVER=dummy SDL_AUDIODRIVER=disk mygame
		// These variable settings are only given to the command, and don't affect the shell or future commands.
		// If we haven't seen any normal arguments yet, check for a =
		if (cmd_line.args.empty)
		{
			if (auto assignment_pieces = piece.findSplit("="))
			{
				cmd_line.tmp_env_vars[assignment_pieces[0]] = Var(assignment_pieces[2]);
				continue;
			}
		}

		// The shell handles globbing.  This toy shell only implements a simple "*" to match all files.
		if (piece == "*")
		{
			copy(dirEntries("", SpanMode.shallow).map!(e => e.name).array.sort, args_app);
			continue;
		}

		// Simplified support for redirects
		if (piece.startsWith("<"))
		{
			cmd_line.io_redir_names[0] = piece[1..$];
			continue;
		}
		if (piece.startsWith(">"))
		{
			cmd_line.io_redir_names[1] = piece[1..$];
			continue;
		}

		// Everything else is just a normal argument
		args_app.put(piece);
	}

	return cmd_line;
}

int runCommand(ref const(CommandLine) cmd_line, VarMap vars)
{
	if (cmd_line.args.length == 0) return vars["?"].value.to!int;

	string cmd_name = cmd_line.args[0];
	auto builtinImpl = cmd_name in builtins;
	if (builtinImpl)
	{
		return (*builtinImpl)(cmd_line, vars);
	}
	else
	{
		return forkAndExec(cmd_line, vars);
	}
}

int forkAndExec(ref const(CommandLine) cmd_line, VarMap vars)
{
	string cmd_name = cmd_line.args[0];

	auto pid = fork();
	if (pid < 0)
	{
		perror("Can't create a new process");
		return ErrorCode.kOther;
	}

	if (pid != 0)
	{
		// Parent process

		// This shell only runs one job at a time, and doesn't do any signal handling.
		// All the parent has to do is wait until the child has finished, then return its exit code.

		// If we don't wait here, the child process and shell will keep running independently.
		// That's how background commands work.
		// A POSIX shell has code here for signal handling and bookkeeping for background commands.

		int status;
		wait(&status);
		return WEXITSTATUS(status);
	}

	// Child process

	// The child process has forked from the shell, so any environment variables set here won't affect the shell.
	foreach (name, var; vars.byPair)
	{
		if (var.exported) environment[name] = var.value;
	}
	foreach (name, var; cmd_line.tmp_env_vars.byPair)
	{
		environment[name] = var.value;
	}

	// The forked child inherits the standard input and output of the parent, as well.
	// At the OS level, standard input is just file descriptor number 0, and standard output is just file descriptor 1.
	// We can redirect the standard input and output of the child just by replacing fd 0 and fd 1 by something else.
	foreach (int fd, string redir_to; cmd_line.io_redir_names)
	{
		if (redir_to !is null)
		{
			// First we need to open a new fd.
			// This shell only supports redirecting to a normally open()ed file.
			// A POSIX shell supports pipelines (like "sort | uniq") by redirecting to fds created with pipe().
			int flags;
			switch (fd)
			{
				case 0:  // Open standard input read-only
					flags = O_RDONLY;
					break;
				case 1:  // Open standard output write-only (and create if it doesn't already exist)
					flags = O_WRONLY | O_CREAT;
					break;
				default:
					assert(false);
			}
			auto redir_fd = open(redir_to.toStringz, flags, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
			if (redir_fd == -1)
			{
				io.stderr.writef("Error opening %s for redirect: ", redir_to);
				perror(null);
				exit(ErrorCode.kOther);
			}

			// A dup2() and close() is used to "move" the fd to where we want it.
			auto err = dup2(redir_fd, fd);
			if (err == -1)
			{
				io.stderr.writef("Error redirecting IO #%d to %s: ", fd, redir_to);
				perror(null);
				exit(ErrorCode.kOther);
			}
			close(redir_fd);
		}
	}

	auto executablez = findExecutable(cmd_name);
	if (executablez is null)
	{
		io.stderr.writef("Command not found: %s\n", cmd_name);
		exit(ErrorCode.kCommandNotFound);
	}

	auto argsz = cmd_line.args.map!toStringz.array ~ null;
	execv(executablez, argsz.ptr);

	// Only get here if exec failed
	io.stderr.writef("Error executing command: %s: ", cmd_name);
	perror(null);
	exit(ErrorCode.kNotExecutable);
	assert (false);
}

// Map a command like "ls" or "./bin/program" to a path to an executable on disk, or return null
const(char*) findExecutable(string cmd)
{
	if (cmd.canFind('/'))
	{
		if (exists(cmd)) return cmd.toStringz();
		return null;
	}

	// Production-worthy shells cache this lookup.
	// They have a "hash" command (or equivalent) for manipulating this cache.
	foreach (dir; environment["PATH"].splitter(":"))
	{
		import std.path : buildPath;
		auto candidate = buildPath(dir, cmd);
		if (exists(candidate)) return candidate.toStringz();
	}
	return null;
}

// We can't fork() and exec() for some commands because they need to change the state of the shell.
// These special-case commands are built into the shell, instead.
// A typical POSIX shell also has some builtins purely for performance reasons (e.g., true and echo).
immutable builtin[string] builtins;
shared static this()
{
	builtins = [
		"=": &varAssignBuiltin,
		"cd": &changeDirBuiltin,
		"exit": &exitBuiltin,
		"export": &exportBuiltin,
	];
}

// An assignment command like "VAR=42" gets lowered to "= VAR 42" (not POSIX)
int varAssignBuiltin(ref const(CommandLine) cmd, VarMap vars)
{
	if (cmd.args.length != 3)
	{
		io.stderr.writeln("Usage: = variable_name value");
		return ErrorCode.kBuiltinUsage;
	}
	auto name = cmd.args[1], value = cmd.args[2];
	auto var = name in vars;
	if (var)
	{
		var.value = value;
	}
	else
	{
		auto exported = name in environment ? Yes.exported : No.exported;
		vars[name] = Var(value, exported);
	}
	return 0;
}

int changeDirBuiltin(ref const(CommandLine) cmd, VarMap vars)
{
	if (cmd.args.length > 2)
	{
		io.stderr.writeln("Usage: cd [dir]");
		return ErrorCode.kBuiltinUsage;
	}

	string dir;
	if (cmd.args.length == 2)
	{
		dir = cmd.args[1];
	}
	else
	{
		dir = getVarValue("HOME", vars, cmd.tmp_env_vars);
	}

	try
	{
		chdir(dir);
	}
	catch (FileException e)
	{
		io.stderr.writef("Error changing directory: %s\n", e.msg);
		return ErrorCode.kOther;
	}
	environment["PWD"] = getcwd();
	return 0;
}

int exitBuiltin(ref const(CommandLine) cmd, VarMap vars)
{
	if (cmd.args.length > 2)
	{
		io.stderr.writeln("Usage: exit [code]");
		return ErrorCode.kBuiltinUsage;
	}
	ubyte status = 0;
	if (cmd.args.length == 2)
	{
		try
		{
			status = cmd.args[1].to!ubyte;
		}
		catch (ConvException e)
		{
			io.stderr.writef("Could not convert %s to status code in range 0-255: %s\n", cmd.args[1], e.msg);
			return ErrorCode.kExitBadArg;
		}
	}
	exit(status);
	assert (false);
}

int exportBuiltin(ref const(CommandLine) cmd, VarMap vars)
{
	if (cmd.args.length != 2)
	{
		io.stderr.writeln("Usage: export variable_name");
		return ErrorCode.kBuiltinUsage;
	}
	auto name = cmd.args[1];
	auto var = name in vars;
	if (var)
	{
		var.exported = Yes.exported;
	}
	else
	{
		vars[name] = Var("", Yes.exported);
	}
	return 0;
}
